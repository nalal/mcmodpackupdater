# __ModPacker__  
____  
__How to set up modpacker for your modpack__  
Setting up ModPacker is relativelly easy to do, all you need is a place to link to your list file as a raw download (IE: `https://yoursite.ext/list`) and MultiMC or any program that loads MineCraft with the ability to run commands on launch.  
  
__Building your `list` file__  
A list file is a simple, plain text file that holds information on how ModPacker should handle your modpack.  
A list file is made up of list entries, each entry is one line formatted as such:  
`<Type>~<URL>~<Version>`  
  
__Type:__  
Type refers to what type of file is being downloaded, you can mark this as `mod` or `cfg`, `mod` will move the file to the mods folder in your minecraft instance, `cfg` will move it to the configs folder in your minecraft instance.  
  
__URL:__  
The url is a simple link to where ModPacker should look to download your files.  
_Note: If your link goes to curseforge or any other CDN based mod repository, you will need to use some trickery to get the actual URL, we'll go over that later._  
  
__Version:__  
Version refers to the version of the mod you intend to download, this isn't too important to get right as it will simply check if this string has changed between downloads. To put it simply, if you want to redownload a file, change this to something different.
  
Here is an example:  
```
mod~https://example.link/mod.jar~1  
cfg~https://example.link/mod.cfg~1
```
_Note that in it's current state, you __MUST__ have the mod name in the URL_ 
  
 
__Custom DIRs:__  
  
Simply add a file to your modpacker directory called `customDIRs.txt` and use the following format to add new DIRs:  
`type~directory`  
`type` refers to the type that will be used in the list and directory is where in the .minecraft folder it needs to be placed.  
Example DIR list and mod list:  
```
rpk~https://example.link/resource_pack.ext~1
```
```
rpk~resourcepacks
```
Keep in mind that `mod` and `cfg` are built in to the program and will automatically point to the `mods` and `config` folder.
