﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

// Filesystem handler
// Deals with directory and file management

namespace PackUpdaterWin
{
    class FileIO
    {
        public static string LocalDIR = Directory.GetCurrentDirectory();
        public static string WorkDIR = Path.Combine(LocalDIR, "modpacker");
        public static string DLCacheDIR = Path.Combine(WorkDIR, "listCache");
        static string DLManifestDIR = Path.Combine(WorkDIR, "listOld");
        static string ModCache = Path.Combine(DLCacheDIR, "mods");
        static string ConfigCache = Path.Combine(DLCacheDIR, "config");
        static string DLCustomDIR = Path.Combine(DLCacheDIR, "custom");
        public static List<DIR> CustomDIRs = new List<DIR>();
        static string listFile = "";
        public struct ListDownload
        {
            public string Type;
            public string URL;
            public string Version;
            public bool Error;
            public ListDownload(string T, string U, string V, bool E)
            {
                Type = T;
                URL = U;
                Version = V;
                Error = E;
            }
        }
        public struct DIR
        {
            public string Type;
            public string CustomDIR;
            public DIR(string T, string D)
            {
                Type = T;
                CustomDIR = D;
            }
        }

        static void InitDIRs()
        {
            if (Directory.Exists(Path.Combine(LocalDIR,"mods")))
            {
                ConsoleOut.Print("Working Directory is " + WorkDIR);
                Directory.CreateDirectory(DLCacheDIR);
                Directory.CreateDirectory(DLManifestDIR);
                Directory.CreateDirectory(ModCache);
                Directory.CreateDirectory(ConfigCache);
                Directory.CreateDirectory(DLCustomDIR);
                GetCustomDirs();
                foreach (DIR i in CustomDIRs)
                {
                    ConsoleOut.Print("Adding custom DIR: " + i.CustomDIR);
                    Directory.CreateDirectory(Path.Combine(i.CustomDIR));
                    Directory.CreateDirectory(Path.Combine(DLCustomDIR, i.Type));
                }
            }
            else
            {
                Err.ErrorInfo("Mods folder could not be found in directory: \n'" + LocalDIR + "'\n" + 
                    "Please make sure you have forge installed.");
                System.Environment.Exit(1);
            }
        }
        public static void LoadDownloads(string URL)
        {
            if (String.URLCut(URL) == "list" || String.URLCut(URL) == "listindev")
            {
                listFile = String.URLCut(URL);
                string ListFileDIR = Path.Combine(DLCacheDIR, String.URLCut(URL));
                Web.Download(URL, DLCacheDIR);
                if (File.Exists(ListFileDIR))
                {
                    string[] ListFile = File.ReadAllLines(ListFileDIR);
                    ExecuteListFile(ListFile);
                }
                else
                {
                    Err.ErrorInfo("Could not find list file " + ListFileDIR);
                }
            }
            else
            {
                Err.ErrorInfo("List file is not in a valid format at URL: \n" + URL + "\n Please make sure your list file is named 'list'.");
                System.Environment.Exit(1);
            }
        }
        public static void GetCustomDirs()
        {
            string[] lines = ReadCustomDirs();
            for(int i = 0; i < lines.Length; i++)
            {
                CustomDIRs.Add(String.GetStringFromDIR(lines[i]));
            }
        }
        public static string[] ReadCustomDirs()
        {
            string dirList = Path.Combine(WorkDIR, "customDIRs.txt");
            string[] CustomDIRLines = { };
            if (File.Exists(dirList))
            {
                ConsoleOut.Print("Reading custom DIRs...");
                CustomDIRLines = File.ReadAllLines(dirList);
                ConsoleOut.Print("Got custom DIRs:");
                foreach (string i in CustomDIRLines)
                {
                    ConsoleOut.Print(i);
                }
            }
            else
            {
                ConsoleOut.Print("No custom DIR list found.");
            }
            return CustomDIRLines;
        }
        public static void ClearPack(bool configs)
        {
            foreach (string f in Directory.GetFiles(Path.Combine(LocalDIR, "mods")))
            {
                File.Delete(f);
            }
            if (configs)
            {
                foreach (string f in Directory.GetFiles(Path.Combine(LocalDIR, "config")))
                {
                    File.Delete(f);
                }
            }
            File.Delete(Path.Combine(DLManifestDIR, "list"));
        }
        static void doThing(ListDownload LD)
        {
            if (!LD.Error)
            {
                string TargetDIR;
                if (LD.Type == "cfg")
                {
                    TargetDIR = ConfigCache;
                    Web.Download(LD.URL, TargetDIR);
                }
                else if (LD.Type == "mod")
                {
                    TargetDIR = ModCache;
                    Web.Download(LD.URL, TargetDIR);
                }
                else if (LD.Type == "del")
                {
                    string removeTarget = String.URLCut(LD.URL);
                    removeTarget = Path.Combine(LocalDIR, "mods", removeTarget);
                    if (File.Exists(removeTarget))
                    {
                        File.Delete(removeTarget);
                    }
                }
                else
                {
                    foreach (DIR i in CustomDIRs)
                    {
                        if (LD.Type == i.Type)
                        {
                            TargetDIR = Path.Combine(DLCustomDIR, i.Type);
                            Web.Download(LD.URL, TargetDIR);
                        }
                    }
                }
            }
        }
        public static void ExecuteListFile(string[] LF)
        {
            
            int i = 0;
            if (File.Exists(Path.Combine(DLManifestDIR, listFile)))
            {
                bool OverRide = false;
                string[] oldCache = File.ReadAllLines(Path.Combine(DLManifestDIR, listFile));
                
                foreach (string S in LF)
                {
                    if (i >= oldCache.Length && !OverRide)
                    {
                        ConsoleOut.Print("New mods found in load order, updating... ");
                        OverRide = true;
                    }
                    if (i >= oldCache.Length)
                    {
                        --i;
                    }
                    ListDownload LD = String.GetStringFromLine(S);
                    ListDownload OLD = String.GetStringFromLine(oldCache[i]);
                    if (OverRide || LD.URL == OLD.URL && LD.Version != OLD.Version)
                    {
                        doThing(LD);
                        ++i;
                    }
                    else if(LD.URL != OLD.URL && !OverRide)
                    {
                        Err.ErrorInfo("Old list formatted differently than new list, overwriting existing mods to prevent errors...");
                        OverRide = true;
                        foreach(string F in Directory.GetFiles(Path.Combine(LocalDIR, "mods")))
                        {
                            File.Delete(F);
                        }
                        doThing(LD);
                        ++i;
                    }
                    else
                    {
                        ConsoleOut.Print("Mod is already installed and at latest version.");
                        ++i;
                    }
                }
                install();
                File.Delete(Path.Combine(DLManifestDIR, listFile));
                File.Copy(Path.Combine(DLCacheDIR, listFile), Path.Combine(DLManifestDIR, listFile));
            }
            else
            {
                foreach (string S in LF)
                {
                    ListDownload LD = String.GetStringFromLine(S);
                    ConsoleOut.Print(LD.Type);
                    doThing(LD);
                }
                install();
                File.Copy(Path.Combine(DLCacheDIR, listFile), Path.Combine(DLManifestDIR, listFile));
            }

        }
        static void install()
        {
            foreach (string f in Directory.GetFiles(ConfigCache))
            {
                ConsoleOut.Print("moving file " + String.FileCut(f) + " to configs.");
                string CPTo = Path.Combine(LocalDIR, "config", String.FileCut(f));
                if (File.Exists(CPTo))
                {
                    ConsoleOut.Print(String.FileCut(f) + " found in mods folder, deleting and moving freshly downloaded version.");
                    File.Delete(CPTo);
                    File.Move(f, CPTo);
                }
                else
                {
                    File.Move(f, CPTo);
                }
            }
            foreach (string f in Directory.GetFiles(ModCache))
            {
                ConsoleOut.Print("moving file " + String.FileCut(f) + " to mods.");
                string CPTo = Path.Combine(LocalDIR, "mods", String.FileCut(f));
                if (File.Exists(CPTo))
                {
                    ConsoleOut.Print(String.FileCut(f) + " found in mods folder, deleting and moving freshly downloaded version.");
                    File.Delete(CPTo);
                    File.Move(f, CPTo);
                }
                else
                {
                    File.Move(f, CPTo);
                }
            }
            foreach(string i in Directory.GetDirectories(DLCustomDIR))
            {
                string custom = String.FileCut(i);
                foreach(DIR directory in CustomDIRs)
                {
                    if (directory.Type == custom)
                    {
                        foreach (string file in Directory.GetFiles(i))
                        {
                            if(File.Exists(Path.Combine(directory.CustomDIR, String.FileCut(file))))
                            {
                                File.Delete(Path.Combine(directory.CustomDIR, String.FileCut(file)));
                            }
                            ConsoleOut.Print("Copying " + file + " to " + directory.CustomDIR);
                            File.Copy(file, Path.Combine(directory.CustomDIR, String.FileCut(file)));
                        }
                    }
                }
            }
        }
        public static void Load()
        {
            if (File.Exists(Path.Combine(DLCacheDIR, listFile)))
            {
                File.Delete(Path.Combine(DLCacheDIR, listFile));
            }
            ConsoleOut.PrintInit("FileIO Loading.");
            ConsoleOut.Print("Running 'InitDIRs'...");
            InitDIRs();
            ConsoleOut.PrintInit("FileIO Loaded.");
        }
        public static string GetLDIR()
        {
            return LocalDIR;
        }
        public static string GetWorkDIR()
        {
            return WorkDIR;
        }
        public static string GetDLDIR()
        {
            return DLCacheDIR;
        }
        public static string GetDLManifestDIR()
        {
            return DLManifestDIR;
        }
    }
}
