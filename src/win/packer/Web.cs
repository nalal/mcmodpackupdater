﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

// Web handler
// Deals with downloads

namespace PackUpdaterWin
{
    class Web
    {
        public static void Load()
        {
            ConsoleOut.PrintInit("Web Loaded.");
        }
        public static void Download(string TargetURL, string TargetDIR)
        {
            ConsoleOut.PrintDL("Downloading file from " + TargetURL + " to " + TargetDIR);
            string FileName = String.URLCut(TargetURL);
            //ConsoleOut.Print("FileName will be " + FileName);
            WebClient wc = new WebClient();
            //ConsoleOut.Print("Download file will be " + TargetDIR + "\\" + FileName);
            wc.DownloadFile(TargetURL, TargetDIR + "\\" + FileName);
        }
        public static void CheckVersion()
        {
            string latestVer = "";
            bool connected = true;
            using (WebClient client = new WebClient())
            {
                try
                {
                    latestVer = client.DownloadString("https://mlp.nac.fluttershub.com:8787/version");
                }
                catch (WebException e)
                {
                    ConsoleOut.PrintErr("Could not connect to update server");
                    ConsoleOut.PrintErr(e.ToString());
                    Program.update = false;
                    connected = false;
                }
            }
            if(connected)
            {
                if(Program.ver == latestVer)
                {
                    ConsoleOut.Print("ModPacker is up to date.");
                }
                else
                {
                    ConsoleOut.Print("Newer version found, updating...");
                }
            }
        }
    }
}
