﻿using System;

/* ============================================================================
 *  PROGRAM:
 *      Minecraft Modpack Update Utility
 *  AUTHOR:
 *      Nac
 *  GROUP:
 *      FTC
 *  INIT DATE:
 *      04/07/2019
 *  DESCRIPTION:
 *      This utility is intended to be used with the utility "MultiMC" though
 *      you may use it however you wish.
 ==============================================================================
 */

    // Main program

namespace PackUpdaterWin
{
    class Program
    {
        public static bool update = true;
        public static string ver = "1.0.0";
        public static string URL;
        static void InitProgram()
        {
            Err.Load();
            FileIO.Load();
            Web.Load();
        }
        static int Main(string[] args)
        {
            Console.WriteLine("[FTC General ModPack Utility]");
            Console.WriteLine("Intended for use with MultiMC.\n");
            if (args.Length == 0)
            {
                ConsoleOut.Print("No console arguments given, terminating peacefully.");
            }
            else
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-h" || args[i] == "--help")
                    {
                        ConsoleOut.Print("USAGE:\n  PackUpdaterWin.exe [options] <URL>");
                        ConsoleOut.Print("    '-c'/'--clear': Clean old list file and download fresh modpack.");
                        ConsoleOut.Print("    '-h'/'--help': Display this help message.");
                        System.Environment.Exit(0);
                    }
                    else if (args[i] == "-c" || args[i] == "--clear")
                    {
                        bool configs = false;
                        ConsoleKey response;
                        do
                        {
                            Console.Write("Remove configs too? [y/n] ");
                            response = Console.ReadKey(false).Key;   // true is intercept key (dont show), false is show
                            if (response != ConsoleKey.Enter)
                                Console.WriteLine();

                        } while (response != ConsoleKey.Y && response != ConsoleKey.N);
                        if (response == ConsoleKey.Y)
                        {
                            configs = true;
                        }
                        ConsoleOut.Print("REMOVING PREVIOUS PACK DATA.");
                        FileIO.ClearPack(configs);
                        System.Environment.Exit(0);
                    }
                    else if (args[i] == "-u" || args[i] == "--url")
                    {
                        ConsoleOut.Print("Got URL: " + args[i + 1]);
                        URL = args[i + 1];
                        i++;
                    }
                    else if (args[i] == "-v" || args[i] == "--version")
                    {
                        ConsoleOut.Print("Program Version: " + ver);
                    }
                }
                InitProgram();
                ConsoleOut.Print("Listing Console Arguments...");
                foreach (string str in args)
                {
                    Console.WriteLine(str);
                }
                if (Err.IsURL(URL))
                {
                    FileIO.LoadDownloads(URL);
                }
                else
                {
                    ConsoleOut.Print(URL + " is not a URL, please supply a valid URL.");
                }
            }
            return 0;
        }
    }
}
