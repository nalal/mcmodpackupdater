﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PackUpdaterWin
{
    class String
    {
        public static string URLCut(string URL)
        {
            string[] BrokenString = URL.Split("/");
            return BrokenString[BrokenString.Length - 1];
        }
        public static string FileCut(string File)
        {
            string[] BrokenString = File.Split("\\");
            return BrokenString[BrokenString.Length - 1];
        }
        public static FileIO.ListDownload GetStringFromLine(string Line)
        {
            string Type;
            string URL;
            string Version;
            bool Error;

            string[] Lines = Line.Split("~");
            if(Lines.Length != 3)
            {
                Err.ErrorInfo("line " + Line + " is formatted incorrectly, Skipping.");
                Type = "ERROR";
                URL = "ERROR";
                Version = "ERROR";
                Error = true;
            }
            else
            {
                Type = Lines[0];
                URL = Lines[1];
                Version = Lines[2];
                Error = false;
            }
            FileIO.ListDownload LD = new FileIO.ListDownload(Type, URL, Version, Error);
            return LD;
        }
        public static FileIO.DIR GetStringFromDIR(string Line)
        {
            string Type;
            string DIR;
            string[] Lines = Line.Split("~");
            if (Lines.Length == 2)
            {
                Type = Lines[0];
                DIR = Path.Combine(FileIO.LocalDIR, Lines[1]);
            }
            else
            {
                Type = "ERROR";
                DIR = "ERROR";
                ConsoleOut.PrintErr("Skipping improperly formatted DIR:");
                ConsoleOut.Print(Line);
            }
            FileIO.DIR CustDIR = new FileIO.DIR(Type, DIR);
            return CustDIR;
        }
    }
}
