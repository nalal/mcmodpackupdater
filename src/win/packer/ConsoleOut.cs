﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PackUpdaterWin
{
    class ConsoleOut
    {
        public static void Print(string Print)
        {
            Console.WriteLine("[INFO]: " + Print);
        }
        public static void PrintInit(string Print)
        {
            Console.WriteLine("[LOADING]: " + Print);
        }
        public static void PrintDL(string Print)
        {
            Console.WriteLine("[DOWNLOAD]: " + Print);
        }
        public static void PrintErr(string Print)
        {
            Console.WriteLine("[!ERROR!]: " + Print);
        }
        public static void PrintWarn(string Print)
        {
            Console.WriteLine("[!WARNING!]: " + Print);
        }
    }
}
