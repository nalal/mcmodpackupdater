﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PackUpdaterWin
{
    class Err
    {
        public static bool IsURL(string URL)
        {
            Uri uriResult;
            bool result = Uri.TryCreate(URL, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            return result;
        }
        public static void Load()
        {
            ConsoleOut.PrintInit("Internal Error Handler Loaded.");
        }
        public static void ErrorInfo(string Error)
        {
            ConsoleOut.PrintErr(Error);
        }
    }
}
