﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;


namespace Updater
{
    class Web
    {
        public static void CheckVersion()
        {
            string latestVer = "";
            bool connected = true;
            using (WebClient client = new WebClient())
            {
                try
                {
                    latestVer = client.DownloadString("https://mlp.nac.fluttershub.com:8787/versionw");
                    ConsoleOut.Print("Got ver " + latestVer);
                }
                catch (WebException e)
                {
                    ConsoleOut.PrintErr("Could not connect to update server.");
                    ConsoleOut.PrintErr(e.ToString());
                    Program.update = false;
                    connected = false;
                }
            }
            if (connected)
            {
                ConsoleOut.Print(Program.ver);
                if (Program.ver == latestVer)
                {
                    ConsoleOut.Print("ModPacker is up to date.");
                    Program.update = false;
                }
                else
                {
                    ConsoleOut.Print("Newer version found, updating...");
                    using (WebClient client = new WebClient())
                    {
                        try
                        {
                            ConsoleOut.Print("Downloading update to " + System.IO.Path.Combine(FileIO.currentDIR, "modpacker", "u.zip"));
                            client.DownloadFile(new Uri("https://mlp.nac.fluttershub.com:8787/PackUpdaterWin/u.zip"), System.IO.Path.Combine(FileIO.currentDIR, "modpacker", "u.zip"));
                            System.IO.File.Delete(System.IO.Path.Combine(FileIO.currentDIR, "modpacker","version.txt"));
                        }
                        catch (WebException e)
                        {
                            Program.update = false;
                            ConsoleOut.PrintErr("Could not download update.");
                            ConsoleOut.PrintErr(e.ToString());
                        }
                    }
                }
            }
        }
    }
}
