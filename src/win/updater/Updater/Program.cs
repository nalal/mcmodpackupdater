﻿using System;

namespace Updater
{
    class Program
    {
        public static string ver;
        public static bool update = false;
        public static bool Debug = true;
        static int Main(string[] args)
        {
            ConsoleOut.Print("Checking for update...");
            string exec = "";
            foreach(string i in args)
            {
                if(i == "-up")
                {
                    update = true;
                }
                else
                {
                    exec = exec + " " + i;
                }
            }
            FileIO.GetCurrentVer();
            Web.CheckVersion();
            FileIO.InstallUpdate();
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = System.IO.Path.Combine(FileIO.currentDIR, "modpacker", "PackUpdaterWin.exe");
            pProcess.StartInfo.Arguments = exec;
            pProcess.StartInfo.UseShellExecute = true;
            pProcess.Start();
            pProcess.WaitForExit();
            return 0;
        }
    }
}
