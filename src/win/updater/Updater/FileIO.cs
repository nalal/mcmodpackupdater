﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Updater
{
    class FileIO
    {
        public static string currentDIR = Directory.GetCurrentDirectory();
        public static void GetCurrentVer()
        {
            if (File.Exists(Path.Combine(currentDIR, "modpacker", "version.txt")))
            {
                string[] Ver = File.ReadAllLines(Path.Combine(currentDIR, "modpacker", "version.txt"));
                Program.ver = Ver[0] + "\n";
            }
            else
            {
                ConsoleOut.Print("Could not find version.txt, forcing update.");
                Program.ver = "-1";
            }
        }
        public static void InstallUpdate()
        {
            ConsoleOut.Print("Updating packer...");
            if(Program.update)
            {
                string[] files = Directory.GetFiles(Path.Combine(currentDIR,"modpacker"));
                foreach(string file in files)
                {
                    ConsoleOut.PrintDebug("REMOVING " + file);
                    if(file.Contains(".") && !file.Contains("u.zip") && !file.Contains("version.txt"))
                    {
                        File.Delete(file);
                    }
                }
                unzipFile(Path.Combine(currentDIR, "modpacker","u.zip"), Path.Combine(currentDIR, "modpacker"));
                File.Delete(Path.Combine(currentDIR, "modpacker", "u.zip"));
            }
        }
        public static void unzipFile(string file, string dest)
        {
            System.IO.Compression.ZipFile.ExtractToDirectory(file, dest);
        }
    }
}
