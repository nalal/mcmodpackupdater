#include "main.h"
#include "download.h"
#include "fileIO.h"

using namespace std;

int main(int argc, char *argv[])
{
	cout << "===ModPacker Utility Loaded===\n";
	cout << "[By Nac/Nalal (FTC Development and System Administration)]\n";
	cout << "[Published under 'GNU General Public License' version 3]\n";
	cout << "Begining opperations...\n";
	cout << "|Running folder check..." << endl;
	if(testDIR() == 1)
	{
		// Check if the correct folders exist in the instance folder
		cout << "Mods folder not found in instance, please make sure you have "
		<< "forge installed." << endl;
		return 1; 
		// Returns 1 for main to prevent MMC loading uninit'd instance
	}
	initFiles();
	if(argc > 1)
	{
		string url = "NONE";
		for(int i = 1; i < argc; i++)
		{
			string buffer = argv[i];
			if(
				buffer.compare("-v") == 0 || 
				buffer.compare("--verbose") == 0)
			{
				makeVerbose();
				cout << "-Curl will return verbose statements.\n";
			}
			else if(buffer.compare("-u") == 0)
			{
				cout << " -Got list URL " << buffer << endl;
				url = argv[i + 1];
				i++;
			}
		}
		if(url.compare("NONE") != 0)
		{
			getDIRs();
			getList(url);
		}
		// Check if program got list on run from arguments
	}
	else
	{
		// If it didn't get a list, check if cache has a list
		cout << "No list URL givem, checking for existing list." << endl;
		if(listExists())
		{
			// It'll do something later, I promise
			cout << "List file found, checking for consistency." << endl;
		}
		else
		{
			// If it doesn't find anything, terminate with no error to load MMC
			cout << "List file not found, launching regardless, no mods " << 
			"changed." << endl;
			return 0;
		}
	}
	cout << "|Program opperations complete." << endl;
	return 0;
}
