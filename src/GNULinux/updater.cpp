#include "updater.h"

using std::cout;
using std::string;

bool update = false;

void runModPacker(string args)
{
	system(args.c_str());
}

void updatePacker()
{
	getVer();
}

int main(uint8_t argc, char *argv[])
{
	cout << "Conocating arg inputs.\n";
	if(unsigned(argc) > 1)
	{
		string out;
		string spacer = " ";
		for(int i = 1; i < unsigned(argc); i++)
		{
			//cout << argv[i] << "\n";
			string buffer = argv[i];
			//cout << buffer << "\n";
			if(buffer.compare("-U") != 0)
			{
				out = out + spacer + buffer;
			}
			else
			{
				update = true;
			}
		}
		string run = "modpacker/packer";
		if(update)
		{
			updatePacker();
		}
		run = run + out;
		runModPacker(run);
	}
	return 0;
}
